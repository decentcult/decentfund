## Digital court
### Kleros
#### The Justice Protocol
A decentralized autonomous organization that delivers
fast, affordable and transparent justice for all. 

[Website](https://kleros.io/) | [Whitepaper](https://kleros.io/assets/whitepaper.pdf) | [Medium](https://medium.com/kleros) | [Twitter](https://twitter.com/kleros_io) | [Github](https://github.com/kleros)
