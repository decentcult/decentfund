## Javascript libraries:

### Ethers.js
Complete Ethereum wallet implementation and utilities in Javascript
[Github](https://github.com/ethers-io/ethers.js/) | [Docs](https://docs.ethers.io/ethers.js/html/)

### Ethereum gasstation server
API Server for the Ethereum Gasstation
[Github](https://github.com/EthereumGasStation/ethereumgasstationserver#api)


## Solidity contracts:

### Yoga token
Jordi Baylina / Giveth token model proposal combining ERC20 and EIP672
[Github](https://github.com/Giveth/yogatoken/)
