# Funds

Фонды бывают двух типов: 
1. Индексные (ходл фонды), там где фонд составляет определенные пропорции активов, которые он хочет держать и в процессе установленного периода старается поддерживать баланс распределения активов.
2. Инвестиционные фонды, где управляющий фондом пытается извлечь максимальную выгоду. 

Обычно есть 4 комиссии, которые платятся для участия в фонде: 
- плата за вход
- плата за выход
- плата за управление
- плата за perfomance (оплачивается при выходе из фонда, составляет разницу между тем, что вынул и вложил)

Обычно определяется период на который минимально можно зайти в фонд (для того, чтобы люди не пытались флиппить).

Исользуя комбинацию комиссий и ограничений во времени, фонды планируют свое управление и соотвествующую стратегию.

В индексных фондах обычно берутся вход или выход + маленький процент (<5%) за управление.
В перфоманс фондах берут вход или выход + высокий перфоманс фи (~20%).

Фонды не раскрывают информацию о том, какие активы они собираются покупать, чтобы избежать front running и открывают только по истечению отчетного периода.

### Примеры
Индексные фонды:
- [Crush Crypto Core](https://www.iconomi.net/dashboard/#/daa/CCC)

Инвестиционные фонды:
- [Pinta](https://www.iconomi.net/dashboard/#/daa/CCC)
