## Zilliqa
Event: Token sale (TBA)

## Bloom
Decentralized credit scoring powered by Ethereum and IPFS

#### Token sale
Token issue: 150 000 000 BLT
Token price: 0.715 USD
Token economy: network currency (identity attesters and risk assesors are setting prices and charging in BLT), voting and invitation system.
Soft cap: 5kk USD
Hard cap: 50kk USD
Dates: 30 Nov 2017 - 1 Jan 2018
Distribution: 50% Crowdsale, 40% Bloom company, 10% Community program.

[Token sale](https://hellobloom.io/token-sale) (30 Nov 2017)

[Calendar event](https://calendar.google.com/event?action=TEMPLATE&tmeid=MjZyOWwzdTNrcTdtZXRzdTAxNXNpNzg3OWQgMWowazJiZjB2a2YzNzJkM2szOTZhaTVqdGtAZw&tmsrc=1j0k2bf0vkf372d3k396ai5jtk%40group.calendar.google.com)

[White paper](https://hellobloom.io/whitepaper.pdf)

[Medium](https://blog.hellobloom.io/)

[Telegram](https://t.me/joinchat/FFWDdQ1hxqIg3jLe7zBVFQ)

[Tokensale contract address](https://etherscan.io/address/0x9d217bcbd0bfae4d7f8f12c7702108d162e3ab79)

## Lendroid
A Non-Rent-Seeking Open Protocol for Decentralized Lending that Enables Margin Trading and Short Selling of ERC20 Tokens
* Partnership with Radar Relay

### Token sale

Token issue: 
[Token sale](https://hellobloom.io/token-sale) (30 Nov 2017)
[Calendar event](https://calendar.google.com/event?action=TEMPLATE&tmeid=MjZyOWwzdTNrcTdtZXRzdTAxNXNpNzg3OWQgMWowazJiZjB2a2YzNzJkM2szOTZhaTVqdGtAZw&tmsrc=1j0k2bf0vkf372d3k396ai5jtk%40group.calendar.google.com)

## Envion
Highly profitable, global crypto-mining-infrastructure - Hosted in mobile, modular CSC containers - Decentralized placement directly at the energy source.

#### Token sale
Token issue: 150 000 000 ENV
Token price: 0.7-1 USD
Token economy: Token holders payouts and voting
Soft cap: 30 000 USD
Dates: 15 Dec - 14 Jan
Distribution: 83% Token holders, 10% Founders, 5% Envion AG, 2% Bounty

[Token sale](https://envion.org/en/ico/) (15 Dec 2017, Presale with discount 30% 1 Dec 2017)

[White paper](https://envion.org/en/download/envion_whitepaper.pdf)

[Medium](https://medium.com/@envion)

[Telegram](https://t.me/Envion)

Tokensale contract address

## SingularityNET
[Token sale](https://hellobloom.io/token-sale) (30 Nov 2017)
[Calendar event](https://calendar.google.com/event?action=TEMPLATE&tmeid=MjZyOWwzdTNrcTdtZXRzdTAxNXNpNzg3OWQgMWowazJiZjB2a2YzNzJkM2szOTZhaTVqdGtAZw&tmsrc=1j0k2bf0vkf372d3k396ai5jtk%40group.calendar.google.com)
