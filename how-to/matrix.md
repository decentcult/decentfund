# Connection
Decent.fund is using matrix for community communication.
To access our channels you should register your account and sign in using our custom server.

Register / login through [http://riot.im/app](riot.im/app) selecting custom server: https://matrix.decent.fund/
You can also download Riot desktop and mobile applications - [https://about.riot.im/downloads/](https://about.riot.im/downloads/)

![](http://decent.fund/static/img/matrix_signup.png)

After signing in you should probably join our community on Riot:
[https://riot.im/app/#/group/+dao:matrix.decent.fund](https://riot.im/app/#/group/+dao:matrix.decent.fund)

For a nicer interface we suggest to use status.im theme:

![](http://storage8.static.itmages.com/i/18/0414/h_1523721703_4259454_a72c26b3de.jpeg)
